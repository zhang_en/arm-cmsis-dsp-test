#include "bitband.h"

void get_mem_infor(u16 * size_sram,u16 * size_flash)
{
	u32 temp1=0;
	temp1=*((vu32 *)(0x1FFFF7E0));
	
	* size_sram=temp1>>16;
	
	* size_flash=temp1&0xffff;
}

void get_uid(u32 *uid)
{
	uid[0]=*((vu32 *)(0x1FFFF7F0));
	uid[1]=*((vu32 *)(0x1FFFF7EC));
	uid[2]=*((vu32 *)(0x1FFFF7E8));
}
