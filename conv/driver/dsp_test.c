#include "dsp_test.h"

float32_t data[64]={0.};		//输入序列1
float32_t data1[32]={0.};		//输入序列2
float32_t data2[95]={0.};	//输出序列

void dsp_test(void)
{
	u8 i=0;
	
	printf("***************\r\n");
	for(i=0;i<64;i++)
	{
		data[i]=arm_sin_f32(100*PI*i/6400);
		
	}
	
	for(i=0;i<64;i++)
	{
		printf("%f\r\n",data[i]);
	}
	
	printf("***************\r\n");
	for(i=0;i<32;i++)
	{
		data1[i]=1;
	}
	
	for(i=0;i<32;i++)
	{
		printf("%f\r\n",data1[i]);
	}
	
	arm_conv_f32(data,64,data1,32,data2);
	printf("***************\r\n");
	for(i=0;i<95;i++)
	{
		printf("%f\r\n",data2[i]);
	}
}

