#include "dsp_test.h"

#define N 128

float x[128] = {0.};
float A, s, X[128];

void dsp_test(void)
{
	int k, n;
	
	for(k=0;k<N;k++)
	{
		x[k]=1.5+sin(100.*PI*k/6400);
	}
	
	for(k = 0; k < N; k++)
	{
		s = 0;
		if(k==0)
			A = sqrt(1.0/N); //计算k=0时的系数
		else
			A = sqrt(2.0/N); //计算k!=0时的系数
		for(n = 0; n < N; n++)
		{
			float tmp = x[n]*cos((PI*(2*n+1)*k)/(2*N));
			s = s + tmp;	//累加求和
		}
         X[k] = A * s;	//X[k]等于累和结果s乘以系数A
	}
      for(k = 0; k < N; k++)  
        printf("%f	%f\r\n",x[k],X[k]);
}

