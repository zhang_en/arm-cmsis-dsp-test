#include "dsp_test.h"


arm_gaussian_naive_bayes_instance_f32 S;

#define SEMIHOSTING 1			//使能
#define NB_OF_CLASSES 3			//类目
#define VECTOR_DIMENSION 2		//向量维数

const float32_t theta[NB_OF_CLASSES*VECTOR_DIMENSION] = {
  1.4539529436590528f, 0.8722776016801852f, 
  -1.5267934452462473f, 0.903204577814203f, 
  -0.15338006360932258f, -2.9997913665803964f
}; /**< 高斯分布的均值 */

const float32_t sigma[NB_OF_CLASSES*VECTOR_DIMENSION] = {
  1.0063470889514925f, 0.9038018246524426f, 
  1.0224479953244736f, 0.7768764290432544f, 
  1.1217662403241206f, 1.2303890106020325f
}; /**< 高斯分布的方差 */

const float32_t classPriors[NB_OF_CLASSES] = {
  0.3333333333333333f, 0.3333333333333333f, 0.3333333333333333f
}; /**< 类先验概率 */


void dsp_test(void)
{
	/* 输入数组 */
  float32_t in[2];

  /* 分类器结果 */
  float32_t result[NB_OF_CLASSES];
  float32_t maxProba;
  uint32_t index;
  
  S.vectorDimension = VECTOR_DIMENSION; 	//向量空间维数
  S.numberOfClasses = NB_OF_CLASSES; 		//不同类数目
  S.theta = theta; 							//高斯分布均值         
  S.sigma = sigma; 							//高斯分布方差        
  S.classPriors = classPriors;  			//先验概率  
  S.epsilon=4.328939296523643e-09f; 		//方差的叠加值

  in[0] = 1.5f;
  in[1] = 1.0f;

  index = arm_gaussian_naive_bayes_predict_f32(&S, in,result);

  maxProba = result[index]; 
#if defined(SEMIHOSTING)
  printf("Class = %d\n", index);
  printf("Max proba = %f\n", (double)maxProba);
#endif

  in[0] = -1.5f;
  in[1] = 1.0f;

  index = arm_gaussian_naive_bayes_predict_f32(&S, in,result);

  maxProba = result[index];
#if defined(SEMIHOSTING)
  printf("Class = %d\n", index);
  printf("Max proba = %f\n", (double)maxProba);
#endif

  in[0] = 0.0f;
  in[1] = -3.0f;

  index = arm_gaussian_naive_bayes_predict_f32(&S, in,result);

  maxProba = result[index];
#if defined(SEMIHOSTING)
  printf("Class = %d\n", index);
  printf("Max proba = %f\n", (double)maxProba);
#endif


}

