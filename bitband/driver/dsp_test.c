#include "dsp_test.h"

void dsp_test(void)
{
	u8 i=0;
	float32_t data[128]={0.};
	for(i=0;i<128;i++)
	{
		data[i]=1.6f+arm_sin_f32(100*PI*i/6400)+0.3f*arm_sin_f32(300*PI*i/6400)+0.5f*arm_sin_f32(700*PI*i/6400);
		printf("%f\r\n",data[i]);
	}
}

